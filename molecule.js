var molecule_syncClosedQuestion = {};
var molecule_syncApplets = {};

function molecule_syncCallback(app, msg) {
    var app_name = app.split('_', 2)[0];

    // Check if a sync is defined for the app_name
    if(molecule_syncClosedQuestion[app_name] != undefined) {
        // Get the identifier of the input field to sync with
        var identifier = molecule_syncClosedQuestion[app_name];

        // Only get the XML in the message
        var message_xml = msg.slice(msg.indexOf("<"), msg.indexOf(">")+1);

        // Get the selected peak from the xml in the message
        var selected_peak = $(message_xml).attr("index");

        // Clear the selected peak if 'baseModel' is present in the message
        // because this would indicate that no peak is selected
        if(msg.indexOf('baseModel') != -1) {
            selected_peak = "";
        }

        // The applet is a stand-alone jmolapplet feed message back so selection does its job
        if(app_name.indexOf('jmolApplet') == 0) {
            back_message = "Select:" + msg.substring(msg.indexOf(":") + 1);
            _jmolFindApplet(app_name).syncScript(back_message);
        }

        // Set the selected peak in the input field
        $('input[name*="_[' + identifier + ']"]').val(selected_peak);
    }
    if(molecule_syncApplets[app_name] != undefined) {
        var receiver_app = molecule_syncApplets[app_name];
        //_jmolFindApplet(receiver_app).syncScript(msg);
        Jmol._applets[molecule_syncApplets[app_name]]._syncScript(msg);
        //Jmol.setAppletSync(Jmol._applets[molecule_syncApplets[app_name]], msg, true);
        //Jmol.setAppletSync(molecule_syncApplets[app_name], msg, true);


        return 1;
    }
}

function molecule_setSyncClosedQuestion(app_name, identifier) {
    molecule_syncClosedQuestion[app_name] = identifier;
}

function molecule_setSyncApplets(spectrum_applet_name, molecule_applet_name) {
    molecule_syncApplets[spectrum_applet_name] = molecule_applet_name;
    molecule_syncApplets[molecule_applet_name] = spectrum_applet_name;
}
